﻿using UnityEngine;

public class PlayerLoseHealth : MonoBehaviour
{   
    [SerializeField] GameObject UIManager;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            UIManager.GetComponent<UIEvent>().UpdateHealth(GameManager.GetInstance().health - 1);
            collision.gameObject.GetComponent<ObjectEvent>().enemyDiedOnHit = true;
            Destroy(collision.gameObject);
        }
    }
}