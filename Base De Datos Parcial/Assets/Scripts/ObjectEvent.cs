﻿using UnityEngine;
using System;

public class ObjectEvent : MonoBehaviour
{
    public static event Action<ObjectEvent> EnemyDie;
    public bool enemyDiedOnHit;
    void OnDestroy()
    {
        if (!enemyDiedOnHit)
        {
            if (EnemyDie != null)
            {
                EnemyDie(this);
            }
        }
    }
}
