﻿using UnityEngine.SceneManagement;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public int maxHealth;
    public string name;
    public int score = 0;
    public bool gameOver;
    public int health;
    #region Singleton
    static private GameManager instance;
    static public GameManager GetInstance() { return instance; }
    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
        if (!instance)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    #endregion
    void Start()
    {
        ResetValues();
    }
    public void WinGame()
    {
        gameOver = true;
        SceneManager.LoadScene("LeaderboardScene");
    }
    public void LoseGame()
    {
        gameOver = true;
        SceneManager.LoadScene("LeaderboardScene");
    }
    void ResetValues()
    {
        health = maxHealth;
        score = 0;
        gameOver = false;
    }
}
