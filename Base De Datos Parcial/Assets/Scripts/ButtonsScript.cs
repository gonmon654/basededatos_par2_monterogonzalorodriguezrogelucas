﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonsScript : MonoBehaviour
{
    public void GoToGame()
    {
        if (GameManager.GetInstance().name != "")
        {
            SceneManager.LoadScene("SampleScene");
        }
        else
        {
            Debug.Log("Tienes que iniciar sesion para jugar");
        }
    }
    public void GoToLeaderBoard()
    {
        SceneManager.LoadScene("LeaderboardScene");
    }
    public void RestartScene()
    {
        SceneManager.LoadScene("SampleScene");
    }
    public void ExitGame()
    {
        Application.Quit();
    }
}
