﻿using System.Collections.Generic;
using UnityEngine;

public class EnemySpawnerAndManager : MonoBehaviour
{
    [SerializeField] int spawnRange;
    [SerializeField] GameObject enemyprefab;
    [SerializeField] int enemyAmount;
    [SerializeField] List<GameObject> enemies;

    private void Start()
    {
        enemies = new List<GameObject>();
        for (short i = 0; i < enemyAmount; i++)
        {
            Vector3 randomSpawnPosition = new Vector3(Random.Range(transform.position.x - spawnRange, transform.position.x + spawnRange),
            1, 
            Random.Range(transform.position.z - spawnRange, transform.position.z + spawnRange));
            GameObject currentEnemy = Instantiate(enemyprefab);
            currentEnemy.transform.position = randomSpawnPosition;
            currentEnemy.transform.parent = transform;
            enemies.Add(currentEnemy);
        }
    }
    public void DestroyEnemy(GameObject enemy)
    {
        enemies.Remove(enemy);
    }
    public void InstanceEnemy()
    {
        Vector3 randomSpawnPosition = new Vector3(Random.Range(transform.position.x - spawnRange, transform.position.x + spawnRange),
        1,
        Random.Range(transform.position.z - spawnRange, transform.position.z + spawnRange));
        GameObject currentEnemy = Instantiate(enemyprefab);
        currentEnemy.transform.position = randomSpawnPosition;
        currentEnemy.transform.parent = transform;
        enemies.Add(currentEnemy);
    }
}
