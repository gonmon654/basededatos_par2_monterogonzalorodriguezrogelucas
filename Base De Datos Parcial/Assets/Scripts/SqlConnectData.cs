﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
public class SqlConnectData : MonoBehaviour
{
    public InputField usernameField;
    public void CallRegister()
    {
        if (usernameField.text != "")
        {
            StartCoroutine(Register());
        }
        else
        {
            Debug.Log("Escribi un nombre");
        }
    }
    IEnumerator Register()
    {
        WWWForm form = new WWWForm();
        form.AddField("username", usernameField.text);
        GameManager.GetInstance().name = usernameField.text;
        form.AddField("score", GameManager.GetInstance().score.ToString());
        UnityWebRequest www = UnityWebRequest.Post("http://localhost/parcialbasededatos/register.php", form);
        yield return www.SendWebRequest();
        if (www.result == UnityWebRequest.Result.Success)
        {
            Debug.Log("cargado y logeado");
        }
    }
}
