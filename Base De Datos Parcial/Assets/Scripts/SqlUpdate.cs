﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

public class SqlUpdate : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            StartCoroutine(UpdateBD());
            //GameManager.GetInstance().WinGame();
        }
    }
    IEnumerator UpdateBD()
    {
        WWWForm form = new WWWForm();
        form.AddField("username", GameManager.GetInstance().name);
        form.AddField("score", GameManager.GetInstance().score);
        UnityWebRequest www = UnityWebRequest.Post("http://localhost/parcialbasededatos/update.php", form);
        yield return www.SendWebRequest();
        if (www.result == UnityWebRequest.Result.Success)
        {
            Debug.Log(www.downloadHandler.text + "cargado");
        }
    }
}
