﻿using UnityEngine;

public class Observer : MonoBehaviour
{
    [SerializeField] GameObject UIManager;
    GameObject enemyManager;
    void Start()
    {
        enemyManager = GameObject.FindGameObjectWithTag("EnemyManager");
        ObjectEvent.EnemyDie += OnObjectDie;
    }
    void OnDestroy()
    {
        ObjectEvent.EnemyDie -= OnObjectDie;
    }
    void OnObjectDie(ObjectEvent objectEvent)
    {
        GameManager.GetInstance().score += 20;
        UIManager.GetComponent<UIEvent>().UpdatePoints(GameManager.GetInstance().score);
        //Enemy Manager reduce enemigo (extract)
        enemyManager.GetComponent<EnemySpawnerAndManager>().DestroyEnemy(objectEvent.gameObject);
        //Enemy Manager respawnea enemigo
        if (!GameManager.GetInstance().gameOver)
        {
            enemyManager.GetComponent<EnemySpawnerAndManager>().InstanceEnemy();
        }
    }
}
