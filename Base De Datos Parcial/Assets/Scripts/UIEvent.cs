﻿using UnityEngine;
using TMPro;

public class UIEvent: MonoBehaviour
{
    [SerializeField] TextMeshProUGUI[] UITexts;
    [SerializeField] float MaxTime;
    private float CurrentTime;

    void Start()
    {
        CurrentTime = MaxTime;
        UpdatePoints(GameManager.GetInstance().score);
        UITexts[0].text = "Health " + GameManager.GetInstance().maxHealth;
    }
    void Update()
    {
        CurrentTime -= Time.deltaTime;
        UITexts[2].text = "Time " + ((int)(CurrentTime)).ToString();
        if (CurrentTime < 0)
        {
            GameManager.GetInstance().LoseGame();
        }
    }
    public void UpdateHealth(int health)
    {
        UITexts[0].text = "Health " + health;
        GameManager.GetInstance().health--;
        if (health <= 0)
        {
            GameManager.GetInstance().LoseGame();
        }
    }
    public void UpdatePoints(int points)
    {
        UITexts[1].text = "Points " + points;
    }
}
