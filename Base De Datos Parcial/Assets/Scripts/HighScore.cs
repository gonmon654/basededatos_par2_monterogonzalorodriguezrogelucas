﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public struct user
{
    public string name;
    public int score;
}
public class HighScore : MonoBehaviour
{

    const int size = 10;
    public Text[] usuarios= new Text[size];
    public Text[] scores = new Text[size];
    private List<user> users= new List<user>();
    public void Start()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        StartCoroutine(Leer());
    }
    IEnumerator Leer()
    {
        WWWForm form = new WWWForm();
        UnityWebRequest web = UnityWebRequest.Post("http://localhost/parcialbasededatos/leer.php", form);
        yield return web.SendWebRequest();
        string[] textoOriginal = web.downloadHandler.text.Split('*', '-');
        for (int i = 0; i < textoOriginal.Length - 1; i += 2)
        {
            user user;
            user.name = textoOriginal[i];
            user.score = int.Parse(textoOriginal[i + 1]);
            users.Add(user);
        }
        users.Sort((x, y) => y.score - x.score);// Esto se llama lambda lambda uwu
        for (int i = 0; i < users.Count; i++)
        {
            usuarios[i].text = users[i].name;
            scores[i].text = users[i].score.ToString();
        }
    }
    public void ReturnToMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
